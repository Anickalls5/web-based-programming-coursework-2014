﻿/*====================================================================================================================================================================================
	THUMBNAIL FUNCTIONS

=> These are the functions that are used for the three thumbnail controls: Left, Right and Close. The thumbnail itself consists of a translucent div that covers the rest of the content. This was modified using the z-index properties in CSS.

=> The close function is straightforward: it simply changes all of the thumnail elements visibility to 'hidden', using the getElementById() method.

=> The left and right functions though are circumstancial. The left button must NOT work on the first image, likewise for the right button on the tenth image. The switch statement below uses the value of the inner HTML code and compares it with each of the ten image variables below. The inner code is retrieved through the html() method. 

=> The actions are a variety: either to show the next or previous image depending on the current position, or to hide the left and right buttons where appropriate.

=> No default values are needed for either switch statement, as the ten images are the only possible values that the user is given.
====================================================================================================================================================================================*/

function thumbnailLEFT(){

var currentImage = $('#thumbnailPhoto').html();
var image01 = '<img src="../static/images/img01.jpg" alt="Photo 1" class="photoH">';
var image02 = '<img src="../static/images/img02.jpg" alt="Photo 2" class="photoV">';
var image03 = '<img src="../static/images/img03.jpg" alt="Photo 3" class="photoV">';
var image04 = '<img src="../static/images/img04.jpg" alt="Photo 4" class="photoH">';
var image05 = '<img src="../static/images/img05.jpg" alt="Photo 5" class="photoH">';
var image06 = '<img src="../static/images/img06.jpg" alt="Photo 6" class="photoV">';
var image07 = '<img src="../static/images/img07.jpg" alt="Photo 7" class="photoH">';
var image08 = '<img src="../static/images/img08.jpg" alt="Photo 8" class="photoV">';
var image09 = '<img src="../static/images/img09.jpg" alt="Photo 9" class="photoV">';
var image10 = '<img src="../static/images/img10.jpg" alt="Photo 10" class="photoH">';

	switch(currentImage){

		case image01:	break;

		case image02:	document.getElementById('controlLEFT').style.visibility = 'hidden';
				$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image01);
			break;

		case image03:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image02);
			break;

		case image04:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image03);
			break;

		case image05:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image04);
			break;

		case image06:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image05);
			break;

		case image07:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image06);
			break;

		case image08:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image07);
			break;

		case image09:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image08);
			break;

		case image10:	document.getElementById('controlRIGHT').style.visibility = 'visible';
				$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image09);
			break;
	};
};



function thumbnailRIGHT(){

var currentImage = $('#thumbnailPhoto').html();
var image01 = '<img src="../static/images/img01.jpg" alt="Photo 1" class="photoH">';
var image02 = '<img src="../static/images/img02.jpg" alt="Photo 2" class="photoV">';
var image03 = '<img src="../static/images/img03.jpg" alt="Photo 3" class="photoV">';
var image04 = '<img src="../static/images/img04.jpg" alt="Photo 4" class="photoH">';
var image05 = '<img src="../static/images/img05.jpg" alt="Photo 5" class="photoH">';
var image06 = '<img src="../static/images/img06.jpg" alt="Photo 6" class="photoV">';
var image07 = '<img src="../static/images/img07.jpg" alt="Photo 7" class="photoH">';
var image08 = '<img src="../static/images/img08.jpg" alt="Photo 8" class="photoV">';
var image09 = '<img src="../static/images/img09.jpg" alt="Photo 9" class="photoV">';
var image10 = '<img src="../static/images/img10.jpg" alt="Photo 10" class="photoH">';

	switch(currentImage){
	
		case image01:	document.getElementById('controlLEFT').style.visibility = 'visible';
				$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image02);
			break;
	
		case image02:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image03);
			break;
	
		case image03:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image04);
			break;
	
		case image04:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image05);
			break;
	
		case image05:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image06);
			break;
	
		case image06:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image07);
			break;
	
		case image07:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image08);
			break;
	
		case image08:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image09);
			break;
	
		case image09:	document.getElementById('controlRIGHT').style.visibility = 'hidden';
				$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image10);
			break;
	
		case image10:	break;
	};

};


function closeThumbnail(){

document.getElementById('thumbnailPhoto').style.visibility = 'hidden';
document.getElementById('thumbnailControls').style.visibility = 'hidden';
document.getElementById('thumbnail').style.visibility = 'hidden';
document.getElementById('controlLEFT').style.visibility = 'hidden';
document.getElementById('controlRIGHT').style.visibility = 'hidden';
document.getElementById('controlCLOSE').style.visibility = 'hidden';
};



/*====================================================================================================================================================================================
	IMAGE FUNCTIONS
	
=> These functions below are simply to display the thumbnail and its components, and then appending the values of the image's HTML code to the thumbnailPhoto div.
====================================================================================================================================================================================*/

var currentImage = $('#thumbnailPhoto').html();
var image01 = '<img src="../static/images/img01.jpg" alt="Photo 1" class="photoH">';
var image02 = '<img src="../static/images/img02.jpg" alt="Photo 2" class="photoV">';
var image03 = '<img src="../static/images/img03.jpg" alt="Photo 3" class="photoV">';
var image04 = '<img src="../static/images/img04.jpg" alt="Photo 4" class="photoH">';
var image05 = '<img src="../static/images/img05.jpg" alt="Photo 5" class="photoH">';
var image06 = '<img src="../static/images/img06.jpg" alt="Photo 6" class="photoV">';
var image07 = '<img src="../static/images/img07.jpg" alt="Photo 7" class="photoH">';
var image08 = '<img src="../static/images/img08.jpg" alt="Photo 8" class="photoV">';
var image09 = '<img src="../static/images/img09.jpg" alt="Photo 9" class="photoV">';
var image10 = '<img src="../static/images/img10.jpg" alt="Photo 10" class="photoH">';



function openImage01(){

document.getElementById('thumbnailPhoto').style.visibility = 'visible';
document.getElementById('thumbnailControls').style.visibility = 'visible';
document.getElementById('controlLEFT').style.visibility = 'hidden';
document.getElementById('controlRIGHT').style.visibility = 'visible';
document.getElementById('controlCLOSE').style.visibility = 'visible';
document.getElementById('thumbnail').style.visibility = 'visible';

$('#thumbnailPhoto').empty();
$('#thumbnailPhoto').append(image01);
};

/*==========================================================*/

function openImage02(){

document.getElementById('thumbnailPhoto').style.visibility = 'visible';
document.getElementById('thumbnailControls').style.visibility = 'visible';
document.getElementById('controlLEFT').style.visibility = 'visible';
document.getElementById('controlRIGHT').style.visibility = 'visible';
document.getElementById('controlCLOSE').style.visibility = 'visible';
document.getElementById('thumbnail').style.visibility = 'visible';

$('#thumbnailPhoto').empty();
$('#thumbnailPhoto').append(image02);
};

/*==========================================================*/

function openImage03(){

document.getElementById('thumbnailPhoto').style.visibility = 'visible';
document.getElementById('thumbnailControls').style.visibility = 'visible';
document.getElementById('controlLEFT').style.visibility = 'visible';
document.getElementById('controlRIGHT').style.visibility = 'visible';
document.getElementById('controlCLOSE').style.visibility = 'visible';
document.getElementById('thumbnail').style.visibility = 'visible';

$('#thumbnailPhoto').empty();
$('#thumbnailPhoto').append(image03);
};

/*==========================================================*/

function openImage04(){

document.getElementById('thumbnailPhoto').style.visibility = 'visible';
document.getElementById('thumbnailControls').style.visibility = 'visible';
document.getElementById('controlLEFT').style.visibility = 'visible';
document.getElementById('controlRIGHT').style.visibility = 'visible';
document.getElementById('controlCLOSE').style.visibility = 'visible';
document.getElementById('thumbnail').style.visibility = 'visible';

$('#thumbnailPhoto').empty();
$('#thumbnailPhoto').append(image04);
};

/*==========================================================*/

function openImage05(){

document.getElementById('thumbnailPhoto').style.visibility = 'visible';
document.getElementById('thumbnailControls').style.visibility = 'visible';
document.getElementById('controlLEFT').style.visibility = 'visible';
document.getElementById('controlRIGHT').style.visibility = 'visible';
document.getElementById('controlCLOSE').style.visibility = 'visible';
document.getElementById('thumbnail').style.visibility = 'visible';

$('#thumbnailPhoto').empty();
$('#thumbnailPhoto').append(image05);
};

/*==========================================================*/

function openImage06(){

document.getElementById('thumbnailPhoto').style.visibility = 'visible';
document.getElementById('thumbnailControls').style.visibility = 'visible';
document.getElementById('controlLEFT').style.visibility = 'visible';
document.getElementById('controlRIGHT').style.visibility = 'visible';
document.getElementById('controlCLOSE').style.visibility = 'visible';
document.getElementById('thumbnail').style.visibility = 'visible';

$('#thumbnailPhoto').empty();
$('#thumbnailPhoto').append(image06);
};

/*==========================================================*/

function openImage07(){

document.getElementById('thumbnailPhoto').style.visibility = 'visible';
document.getElementById('thumbnailControls').style.visibility = 'visible';
document.getElementById('controlLEFT').style.visibility = 'visible';
document.getElementById('controlRIGHT').style.visibility = 'visible';
document.getElementById('controlCLOSE').style.visibility = 'visible';
document.getElementById('thumbnail').style.visibility = 'visible';

$('#thumbnailPhoto').empty();
$('#thumbnailPhoto').append(image07);
};

/*==========================================================*/

function openImage08(){

document.getElementById('thumbnailPhoto').style.visibility = 'visible';
document.getElementById('thumbnailControls').style.visibility = 'visible';
document.getElementById('controlLEFT').style.visibility = 'visible';
document.getElementById('controlRIGHT').style.visibility = 'visible';
document.getElementById('controlCLOSE').style.visibility = 'visible';
document.getElementById('thumbnail').style.visibility = 'visible';

$('#thumbnailPhoto').empty();
$('#thumbnailPhoto').append(image08);
};

/*==========================================================*/

function openImage09(){

document.getElementById('thumbnailPhoto').style.visibility = 'visible';
document.getElementById('thumbnailControls').style.visibility = 'visible';
document.getElementById('controlLEFT').style.visibility = 'visible';
document.getElementById('controlRIGHT').style.visibility = 'visible';
document.getElementById('controlCLOSE').style.visibility = 'visible';
document.getElementById('thumbnail').style.visibility = 'visible';

$('#thumbnailPhoto').empty();
$('#thumbnailPhoto').append(image09);
};

/*==========================================================*/

function openImage10(){

document.getElementById('thumbnailPhoto').style.visibility = 'visible';
document.getElementById('thumbnailControls').style.visibility = 'visible';
document.getElementById('controlLEFT').style.visibility = 'visible';
document.getElementById('controlRIGHT').style.visibility = 'hidden';
document.getElementById('controlCLOSE').style.visibility = 'visible';
document.getElementById('thumbnail').style.visibility = 'visible';

$('#thumbnailPhoto').empty();
$('#thumbnailPhoto').append(image10);
};



/*====================================================================================================================================================================================
	CALENDAR FUNCTIONS

=> The main feature of the booking page is the changeable calendar. There are several functions associated with it to ensure it works correctly:


=> clearCalendar() is a simple sub-function that clears all items with the classes from week 1 to week 6, which are all of the divs within the calendar's structure. This function saves many lines of code being re-written.


=> getFullDate() is a function that takes a date parameter and returns it in the string format: "YYYY-MM-DD". This allows it to be used multiple ways by other functions. getFullDate() retrieves the day, month and year through simple accessor methods (converting them to strings accordingly) and concatenates them together.


=> todayDate() is another sub-function that returns the value of today's date. It uses the other function: getFullDate(), so the two must be defined together.


=> getDayClass is a sub-function that will take four internal parameters: a date, a month, a year and an id number. IT MUST BE NOTED THAT THIS FUNCTION IS PRIMARILY USED FOR USE IN THE loadCalendar() FOR LOOP FUNCTION, OTHER USES ARE NOT RECOMMENDED.

   ~ Using the parameters, the function will retrieve the value of the current day selected in the format YYYY-MM-DD. The Python variables given in the html are stored in specific divs as the inner elements of an array. The JSON.parse method will convert this to a Javascript array, which will then be read appropriately.

   ~ If the current date established is located in the array containing the list of unavailable dates (giving a index number, otherwise -1 will be returned), and the value of the element in the SAME LOCATION (found by transferring the index number of the date to the second array of availabilities) within the second array is "Y", then add a new class, displaying the day's div (given as the idNumber) as an unavailable day.

   ~ If, however, the current date is contained within the array, BUT the value of the element in the second array is equal to "N", then the class added will be that of 'requested', else the class 'available' will be added.

   ~ If the date matches today's date (using the todayDate() function), then a simple class is added to display the number differently.


=> The main function of the calendar is the loadCalendar() function, which will load the visual elements of the calendar corresponding to the value selected in the drop-down menu. The function is composed of a single switch statement, made up of 12 cases (one for each month). The variables monthValue and dateNo are all defined beforehand.

   ~ Each case in the statement will call the clearCalendar() function, so that there are no overlapping elements (as these are all divs). The dayNo represents the DAY in which the month starts (1 - Monday, 2 - Tuesday...), this is essentially the number of days to start counting from. The monthNo and yearNo are also established as STRINGS.

   ~ The formatting classes (unavailable, available, accepted and current) are then removed from all of the elements that contain those classes (essentially removing all of them).

   ~ The for loop is the central body of each case. Starting at the value of dayNo, with a post-actiong of dayNo++, the loop will append the value of dateNo (always starting from 1) to each element with the id #dayN (where n is a number, e.g. day0, day1, day2 are all ids of div elements). The value of dateNo is appended to this div, and the getDayClass() method is applied to find whether the date (yearNo + monthNo + dateNo) is either available, requested or unavailable (or current). After each iteration, dateNo is incremented by 1. This case action is repeated for all cases within the switch statement, with changing dayNo, monthNo and yearNo values accordingly.


=> The checkDate() function is used within the form. It's main task is to check whether the date has been previously booked OR requested. If so, the form WILL NOT submit. The comparison statements from the getDayClass() function are used here. This function returns either true or false.


=> Finally, we have the getCost() function, which simply sets the value of the constant COST_SEASON according to the value of MonthSelect (the same drop-down used by loadCalendar(). The values of COST_SEASON are 50, 60 and 65, and multiplied by the duration (also retrieved as a variable), the total cost will be calculated and send to a hidden input field within the form (so that it can be, in turn, sent to the Python script).

====================================================================================================================================================================================*/

function clearCalendar(){
   $(".week1").empty();
   $(".week2").empty();
   $(".week3").empty();
   $(".week4").empty();
   $(".week5").empty();
   $(".week6").empty();
}


function getFullDate(dateObject){

	var year = dateObject.getFullYear();
	var month = dateObject.getMonth()+1;
	var day = dateObject.getDate();


	if(month < 10){
	   month = '0'+String(month);
	
	}else{
	   month = String(month);
	}


	if(day < 10){
	   day = '0'+String(day);
	
	}else{
	   day = String(day);
	}


	var fullDate = String(year)+'-'+month+'-'+day;

   return fullDate;

}


function todayDate(){

   var t = new Date();
   var todayDate = getFullDate(t);

   document.getElementById("dateDropSelect").value = todayDate;

   return todayDate;

}



function getDayClass(date, month, year, id){

   dateNo = date;
   monthNo = month;
   yearNo = year;
   idNumber = id;

   if(dateNo<10){
      dateNo = "0"+dateNo;
   }

   var fullDate = yearNo+'-'+monthNo+'-'+dateNo;

   var pythonVar1 = $('#dateListDiv').html();
   var dateList = JSON.parse("["+pythonVar1+"]");

   var pythonVar2 = $('#confirmListDiv').html();
   var confirmList = JSON.parse("["+pythonVar2+"]");


   if(fullDate == todayDate()){
      $(idNumber).addClass("currentDay");
   }


   if(dateList.indexOf(String(fullDate)) == -1){
      $(idNumber).removeClass("availableDay");
      $(idNumber).removeClass("acceptedDay");
      $(idNumber).removeClass("unavailableDay");

      $(idNumber).addClass("availableDay");

   }else if(dateList.indexOf(String(fullDate)) != -1 && confirmList[dateList.indexOf(String(fullDate))] == "N"){
      $(idNumber).removeClass("availableDay");
      $(idNumber).removeClass("acceptedDay");
      $(idNumber).removeClass("unavailableDay");

      $(idNumber).addClass("acceptedDay");
   
   }else if(dateList.indexOf(String(fullDate)) != -1 && confirmList[dateList.indexOf(String(fullDate))] == "Y"){
      $(idNumber).removeClass("availableDay");
      $(idNumber).removeClass("acceptedDay");
      $(idNumber).removeClass("unavailableDay");

      $(idNumber).addClass("unavailableDay");

   }

}


function loadCalendar(){

monthValue = $('#calendarMonth').val();
dateNo = 1;

	switch(monthValue){

		case 'Dec 2014': clearCalendar();
			
				 dayNo = 1;

				 monthNo = '12';
				 yearNo = '2014';

				 $(".availableDay").removeClass("availableDay");
				 $(".acceptedDay").removeClass("acceptedDay");
				 $(".unavailableDay").removeClass("unavailableDay");
				 $(".currentDay").removeClass("currentDay");

				 for(dayNo; dateNo<=31; dayNo++){
				    idNumber = '#day'+dayNo;
				    $(idNumber).append(dateNo);

				    getDayClass(dateNo, monthNo, yearNo, idNumber);
				    
				    dateNo++;
				 }

				 break;

		/*====================*/
		case 'Jan 2015': clearCalendar();
		
				 dayNo = 4;

				 monthNo = '01';
				 yearNo = 2015;

				 $(".availableDay").removeClass("availableDay");
				 $(".acceptedDay").removeClass("acceptedDay");
				 $(".unavailableDay").removeClass("unavailableDay");
				 $(".currentDay").removeClass("currentDay");

				 for(dayNo; dateNo<=31; dayNo++){
				    idNumber = '#day'+dayNo;
				    $(idNumber).append(dateNo);

				    getDayClass(dateNo, monthNo, yearNo, idNumber);

				    dateNo++;
		                 }
			 
   				 break;

		/*====================*/
		case 'Feb 2015': clearCalendar();
		
				 dayNo = 7;

				 monthNo = '02';
				 yearNo = 2015;

				 $(".availableDay").removeClass("availableDay");
				 $(".acceptedDay").removeClass("acceptedDay");
				 $(".unavailableDay").removeClass("unavailableDay");
				 $(".currentDay").removeClass("currentDay");

				 for(dayNo; dateNo<=28; dayNo++){
				    idNumber = '#day'+dayNo;
				    $(idNumber).append(dateNo);

				    getDayClass(dateNo, monthNo, yearNo, idNumber);

				    dateNo++;
				 }
				 
				 break;
	
		/*====================*/
		case 'Mar 2015': clearCalendar();
			
				 dayNo = 7;

				 monthNo = '03';
				 yearNo = 2015;

				 $(".availableDay").removeClass("availableDay");
				 $(".acceptedDay").removeClass("acceptedDay");
				 $(".unavailableDay").removeClass("unavailableDay");
				 $(".currentDay").removeClass("currentDay");

				 for(dayNo; dateNo<=31; dayNo++){
				    idNumber = '#day'+dayNo;
				    $(idNumber).append(dateNo);

				    getDayClass(dateNo, monthNo, yearNo, idNumber);

				    dateNo++;
				 }
				 
				 break;
	
		/*====================*/
		case 'Apr 2015': clearCalendar();
	
				 dayNo = 3;

				 monthNo = '04';
				 yearNo = 2015;

				 $(".availableDay").removeClass("availableDay");
				 $(".acceptedDay").removeClass("acceptedDay");
				 $(".unavailableDay").removeClass("unavailableDay");
				 $(".currentDay").removeClass("currentDay");

				 for(dayNo; dateNo<=30; dayNo++){
				    idNumber = '#day'+dayNo;
				    $(idNumber).append(dateNo);

				    getDayClass(dateNo, monthNo, yearNo, idNumber);

				    dateNo++;
				 }
			 
				 break;

		/*====================*/
		case 'May 2015': clearCalendar();
		
				 dayNo = 5;

				 monthNo = '05';
				 yearNo = 2015;

				 $(".availableDay").removeClass("availableDay");
				 $(".acceptedDay").removeClass("acceptedDay");
				 $(".unavailableDay").removeClass("unavailableDay");
				 $(".currentDay").removeClass("currentDay");

				 for(dayNo; dateNo<=31; dayNo++){
				    idNumber = '#day'+dayNo;
				    $(idNumber).append(dateNo);

				    getDayClass(dateNo, monthNo, yearNo, idNumber);

				    dateNo++;
				 }
			 
				 break;

		/*====================*/
		case 'Jun 2015': clearCalendar();
		
				 dayNo = 1;

				 monthNo = '06';
				 yearNo = 2015;

				 $(".availableDay").removeClass("availableDay");
				 $(".acceptedDay").removeClass("acceptedDay");
				 $(".unavailableDay").removeClass("unavailableDay");
				 $(".currentDay").removeClass("currentDay");
	
				 for(dayNo; dateNo<=30; dayNo++){
				    idNumber = '#day'+dayNo;
				    $(idNumber).append(dateNo);

				    getDayClass(dateNo, monthNo, yearNo, idNumber);

				    dateNo++;
				 }
			 
				 break;

		/*====================*/
		case 'Jul 2015': clearCalendar();
			
				 dayNo = 3;

				 monthNo = '07';
				 yearNo = 2015;

				 $(".availableDay").removeClass("availableDay");
				 $(".acceptedDay").removeClass("acceptedDay");
				 $(".unavailableDay").removeClass("unavailableDay");
				 $(".currentDay").removeClass("currentDay");
				
				 for(dayNo; dateNo<=31; dayNo++){
				    idNumber = '#day'+dayNo;
				    $(idNumber).append(dateNo);

				    getDayClass(dateNo, monthNo, yearNo, idNumber);

				    dateNo++;
			         }
			 
			         break;

		/*====================*/
		case 'Aug 2015': clearCalendar();
		
				 dayNo = 6;

				 monthNo = '08';
				 yearNo = 2015;

				 $(".availableDay").removeClass("availableDay");
				 $(".acceptedDay").removeClass("acceptedDay");
				 $(".unavailableDay").removeClass("unavailableDay");
				 $(".currentDay").removeClass("currentDay");
				
				 for(dayNo; dateNo<=31; dayNo++){
				    idNumber = '#day'+dayNo;
				    $(idNumber).append(dateNo);

				    getDayClass(dateNo, monthNo, yearNo, idNumber);

				    dateNo++;
				 }
			 
				 break;

		/*====================*/
		case 'Sep 2015': clearCalendar();

				 dayNo = 2;

				 monthNo = '09';
				 yearNo = 2015;

				 $(".availableDay").removeClass("availableDay");
				 $(".acceptedDay").removeClass("acceptedDay");
				 $(".unavailableDay").removeClass("unavailableDay");
				 $(".currentDay").removeClass("currentDay");
			
				 for(dayNo; dateNo<=30; dayNo++){
				    idNumber = '#day'+dayNo;
				    $(idNumber).append(dateNo);

				    getDayClass(dateNo, monthNo, yearNo, idNumber);

				    dateNo++;
				 }
				 
				 break;

		/*====================*/
		case 'Oct 2015': clearCalendar();
	
				 dayNo = 4;

				 monthNo = '10';
				 yearNo = 2015;

				 $(".availableDay").removeClass("availableDay");
				 $(".acceptedDay").removeClass("acceptedDay");
				 $(".unavailableDay").removeClass("unavailableDay");
				 $(".currentDay").removeClass("currentDay");
			
				 for(dayNo; dateNo<=31; dayNo++){
				    idNumber = '#day'+dayNo;
				    $(idNumber).append(dateNo);

				    getDayClass(dateNo, monthNo, yearNo, idNumber);

				    dateNo++;
				 }
			 
			 	 break;

		/*====================*/
		case 'Nov 2015': clearCalendar();
		
				 dayNo = 7;

				 monthNo = '11';
				 yearNo = 2015;

				 $(".availableDay").removeClass("availableDay");
				 $(".acceptedDay").removeClass("acceptedDay");
				 $(".unavailableDay").removeClass("unavailableDay");
				 $(".currentDay").removeClass("currentDay");
				
				 for(dayNo; dateNo<=30; dayNo++){
				    idNumber = '#day'+dayNo;
				    $(idNumber).append(dateNo);

				    getDayClass(dateNo, monthNo, yearNo, idNumber);

				    dateNo++;
				 }
				 
				 break;
	
		/*====================*/
		case 'Dec 2015': clearCalendar();
	
				 dayNo = 2;

				 monthNo = '12';
				 yearNo = 2015;

				 $(".availableDay").removeClass("availableDay");
				 $(".acceptedDay").removeClass("acceptedDay");
				 $(".unavailableDay").removeClass("unavailableDay");
				 $(".currentDay").removeClass("currentDay");			 
			
				 for(dayNo; dateNo<=31; dayNo++){
				    idNumber = '#day'+dayNo;
				    $(idNumber).append(dateNo);

				    getDayClass(dateNo, monthNo, yearNo, idNumber);

				    dateNo++;
				 }
		 
				 break;

		}

}



function checkDate(){

	var startDate;
	var duration = $('#durationSelect').val();

	var pythonVar1 = $('#dateListDiv').html();
	var dateList = JSON.parse("["+pythonVar1+"]");

	var pythonVar2 = $('#confirmListDiv').html();
	var confirmList = JSON.parse("["+pythonVar2+"]");


	for(n=0; n<=duration-1; n++){

	   startDate = new Date($('#dateDropSelect').val());
	   dateObject = new Date(startDate.setDate(startDate.getDate()+n));

	   dateElement = getFullDate(dateObject);

	   if(dateList.indexOf(dateElement) != -1){
	      alert('Sorry, this date is either already booked, or is in the process of being requested. Please check the calendar for available dates.');
	      return false;
	      break;

	   }else{
	      return true;  
	   }

	}
}



function getCost(){

	var inputDate = new Date($('#dateDropSelect').val());
	var inputDuration = $('#durationSelect').val();
	var seasonMonth = inputDate.getMonth()+1;

	var SEASON_COST = 0;

	switch(seasonMonth){

	   case 1: 	SEASON_COST = 60;
			$('#totalCost').empty();
			$('#totalCost').append("£"+inputDuration*SEASON_COST+".00");
			$('input[name=costSeason]').val(SEASON_COST);
			break;

	   case 2: 	SEASON_COST = 60;
			$('#totalCost').empty();
			$('#totalCost').append("£"+inputDuration*SEASON_COST+".00");
			$('input[name=costSeason]').val(SEASON_COST);
			break;

	   case 3: 	SEASON_COST = 50;
			$('#totalCost').empty();
			$('#totalCost').append("£"+inputDuration*SEASON_COST+".00");
			$('input[name=costSeason]').val(SEASON_COST);
			break;

	   case 4: 	SEASON_COST = 50;
			$('#totalCost').empty();
			$('#totalCost').append("£"+inputDuration*SEASON_COST+".00");
			$('input[name=costSeason]').val(SEASON_COST);
			break;

	   case 5: 	SEASON_COST = 50;
			$('#totalCost').empty();
			$('#totalCost').append("£"+inputDuration*SEASON_COST+".00");
			$('input[name=costSeason]').val(SEASON_COST);
			break;

	   case 6: 	SEASON_COST = 65;
			$('#totalCost').empty();
			$('#totalCost').append("£"+inputDuration*SEASON_COST+".00");
			$('input[name=costSeason]').val(SEASON_COST);
			break;

	   case 7: 	SEASON_COST = 65;
			$('#totalCost').empty();
			$('#totalCost').append("£"+inputDuration*SEASON_COST+".00");
			$('input[name=costSeason]').val(SEASON_COST);
			break;

	   case 8: 	SEASON_COST = 65;
			$('#totalCost').empty();
			$('#totalCost').append("£"+inputDuration*SEASON_COST+".00");
			$('input[name=costSeason]').val(SEASON_COST);
			break;

	   case 9: 	SEASON_COST = 50;
			$('#totalCost').empty();
			$('#totalCost').append("£"+inputDuration*SEASON_COST+".00");
			$('input[name=costSeason]').val(SEASON_COST);
			break;

	   case 10: 	SEASON_COST = 50;
			$('#totalCost').empty();
			$('#totalCost').append("£"+inputDuration*SEASON_COST+".00");
			$('input[name=costSeason]').val(SEASON_COST);
			break;

	   case 11: 	SEASON_COST = 50;
			$('#totalCost').empty();
			$('#totalCost').append("£"+inputDuration*SEASON_COST+".00");
			$('input[name=costSeason]').val(SEASON_COST);
			break;

	   case 12: 	SEASON_COST = 60;
			$('#totalCost').empty();
			$('#totalCost').append("£"+inputDuration*SEASON_COST+".00");
			$('input[name=costSeason]').val(SEASON_COST);
			break;
	   }
}