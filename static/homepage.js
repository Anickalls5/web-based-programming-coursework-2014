﻿/*====================================================================================================================================================================================
	THUMBNAIL FUNCTIONS

=> These are the functions that are used for the three thumbnail controls: Left, Right and Close. The thumbnail itself consists of a translucent div that covers the rest of the content. This was modified using the z-index properties in CSS.

=> The close function is straightforward: it simply changes all of the thumnail elements visibility to 'hidden', using the getElementById() method.

=> The left and right functions though are circumstancial. The left button must NOT work on the first image, likewise for the right button on the tenth image. The switch statement below uses the value of the inner HTML code and compares it with each of the ten image variables below. The inner code is retrieved through the html() method. 

=> The actions are a variety: either to show the next or previous image depending on the current position, or to hide the left and right buttons where appropriate.

=> No default values are needed for either switch statement, as the ten images are the only possible values that the user is given.
====================================================================================================================================================================================*/

function thumbnailLEFT(){

var currentImage = $('#thumbnailPhoto').html();
var image01 = '<img src="../static/images/img01.jpg" alt="Photo 1" class="photoH">';
var image02 = '<img src="../static/images/img02.jpg" alt="Photo 2" class="photoV">';
var image03 = '<img src="../static/images/img03.jpg" alt="Photo 3" class="photoV">';
var image04 = '<img src="../static/images/img04.jpg" alt="Photo 4" class="photoH">';
var image05 = '<img src="../static/images/img05.jpg" alt="Photo 5" class="photoH">';
var image06 = '<img src="../static/images/img06.jpg" alt="Photo 6" class="photoV">';
var image07 = '<img src="../static/images/img07.jpg" alt="Photo 7" class="photoH">';
var image08 = '<img src="../static/images/img08.jpg" alt="Photo 8" class="photoV">';
var image09 = '<img src="../static/images/img09.jpg" alt="Photo 9" class="photoV">';
var image10 = '<img src="../static/images/img10.jpg" alt="Photo 10" class="photoH">';

	switch(currentImage){

		case image01:	break;

		case image02:	document.getElementById('controlLEFT').style.visibility = 'hidden';
				$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image01);
			break;

		case image03:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image02);
			break;

		case image04:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image03);
			break;

		case image05:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image04);
			break;

		case image06:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image05);
			break;

		case image07:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image06);
			break;

		case image08:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image07);
			break;

		case image09:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image08);
			break;

		case image10:	document.getElementById('controlRIGHT').style.visibility = 'visible';
				$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image09);
			break;
	};
};



function thumbnailRIGHT(){

var currentImage = $('#thumbnailPhoto').html();
var image01 = '<img src="../static/images/img01.jpg" alt="Photo 1" class="photoH">';
var image02 = '<img src="../static/images/img02.jpg" alt="Photo 2" class="photoV">';
var image03 = '<img src="../static/images/img03.jpg" alt="Photo 3" class="photoV">';
var image04 = '<img src="../static/images/img04.jpg" alt="Photo 4" class="photoH">';
var image05 = '<img src="../static/images/img05.jpg" alt="Photo 5" class="photoH">';
var image06 = '<img src="../static/images/img06.jpg" alt="Photo 6" class="photoV">';
var image07 = '<img src="../static/images/img07.jpg" alt="Photo 7" class="photoH">';
var image08 = '<img src="../static/images/img08.jpg" alt="Photo 8" class="photoV">';
var image09 = '<img src="../static/images/img09.jpg" alt="Photo 9" class="photoV">';
var image10 = '<img src="../static/images/img10.jpg" alt="Photo 10" class="photoH">';

	switch(currentImage){
	
		case image01:	document.getElementById('controlLEFT').style.visibility = 'visible';
				$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image02);
			break;
	
		case image02:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image03);
			break;
	
		case image03:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image04);
			break;
	
		case image04:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image05);
			break;
	
		case image05:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image06);
			break;
	
		case image06:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image07);
			break;
	
		case image07:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image08);
			break;
	
		case image08:	$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image09);
			break;
	
		case image09:	document.getElementById('controlRIGHT').style.visibility = 'hidden';
				$('#thumbnailPhoto').empty();
				$('#thumbnailPhoto').append(image10);
			break;
	
		case image10:	break;
	};

};


function closeThumbnail(){

document.getElementById('thumbnailPhoto').style.visibility = 'hidden';
document.getElementById('thumbnailControls').style.visibility = 'hidden';
document.getElementById('thumbnail').style.visibility = 'hidden';
document.getElementById('controlLEFT').style.visibility = 'hidden';
document.getElementById('controlRIGHT').style.visibility = 'hidden';
document.getElementById('controlCLOSE').style.visibility = 'hidden';
};



/*====================================================================================================================================================================================
	IMAGE FUNCTIONS
	
=> These functions below are simply to display the thumbnail and its components, and then appending the values of the image's HTML code to the thumbnailPhoto div.
====================================================================================================================================================================================*/

var currentImage = $('#thumbnailPhoto').html();
var image01 = '<img src="../static/images/img01.jpg" alt="Photo 1" class="photoH">';
var image02 = '<img src="../static/images/img02.jpg" alt="Photo 2" class="photoV">';
var image03 = '<img src="../static/images/img03.jpg" alt="Photo 3" class="photoV">';
var image04 = '<img src="../static/images/img04.jpg" alt="Photo 4" class="photoH">';
var image05 = '<img src="../static/images/img05.jpg" alt="Photo 5" class="photoH">';
var image06 = '<img src="../static/images/img06.jpg" alt="Photo 6" class="photoV">';
var image07 = '<img src="../static/images/img07.jpg" alt="Photo 7" class="photoH">';
var image08 = '<img src="../static/images/img08.jpg" alt="Photo 8" class="photoV">';
var image09 = '<img src="../static/images/img09.jpg" alt="Photo 9" class="photoV">';
var image10 = '<img src="../static/images/img10.jpg" alt="Photo 10" class="photoH">';



function openImage01(){

document.getElementById('thumbnailPhoto').style.visibility = 'visible';
document.getElementById('thumbnailControls').style.visibility = 'visible';
document.getElementById('controlLEFT').style.visibility = 'hidden';
document.getElementById('controlRIGHT').style.visibility = 'visible';
document.getElementById('controlCLOSE').style.visibility = 'visible';
document.getElementById('thumbnail').style.visibility = 'visible';

$('#thumbnailPhoto').empty();
$('#thumbnailPhoto').append(image01);
};

/*==========================================================*/

function openImage02(){

document.getElementById('thumbnailPhoto').style.visibility = 'visible';
document.getElementById('thumbnailControls').style.visibility = 'visible';
document.getElementById('controlLEFT').style.visibility = 'visible';
document.getElementById('controlRIGHT').style.visibility = 'visible';
document.getElementById('controlCLOSE').style.visibility = 'visible';
document.getElementById('thumbnail').style.visibility = 'visible';

$('#thumbnailPhoto').empty();
$('#thumbnailPhoto').append(image02);
};

/*==========================================================*/

function openImage03(){

document.getElementById('thumbnailPhoto').style.visibility = 'visible';
document.getElementById('thumbnailControls').style.visibility = 'visible';
document.getElementById('controlLEFT').style.visibility = 'visible';
document.getElementById('controlRIGHT').style.visibility = 'visible';
document.getElementById('controlCLOSE').style.visibility = 'visible';
document.getElementById('thumbnail').style.visibility = 'visible';

$('#thumbnailPhoto').empty();
$('#thumbnailPhoto').append(image03);
};

/*==========================================================*/

function openImage04(){

document.getElementById('thumbnailPhoto').style.visibility = 'visible';
document.getElementById('thumbnailControls').style.visibility = 'visible';
document.getElementById('controlLEFT').style.visibility = 'visible';
document.getElementById('controlRIGHT').style.visibility = 'visible';
document.getElementById('controlCLOSE').style.visibility = 'visible';
document.getElementById('thumbnail').style.visibility = 'visible';

$('#thumbnailPhoto').empty();
$('#thumbnailPhoto').append(image04);
};

/*==========================================================*/

function openImage05(){

document.getElementById('thumbnailPhoto').style.visibility = 'visible';
document.getElementById('thumbnailControls').style.visibility = 'visible';
document.getElementById('controlLEFT').style.visibility = 'visible';
document.getElementById('controlRIGHT').style.visibility = 'visible';
document.getElementById('controlCLOSE').style.visibility = 'visible';
document.getElementById('thumbnail').style.visibility = 'visible';

$('#thumbnailPhoto').empty();
$('#thumbnailPhoto').append(image05);
};

/*==========================================================*/

function openImage06(){

document.getElementById('thumbnailPhoto').style.visibility = 'visible';
document.getElementById('thumbnailControls').style.visibility = 'visible';
document.getElementById('controlLEFT').style.visibility = 'visible';
document.getElementById('controlRIGHT').style.visibility = 'visible';
document.getElementById('controlCLOSE').style.visibility = 'visible';
document.getElementById('thumbnail').style.visibility = 'visible';

$('#thumbnailPhoto').empty();
$('#thumbnailPhoto').append(image06);
};

/*==========================================================*/

function openImage07(){

document.getElementById('thumbnailPhoto').style.visibility = 'visible';
document.getElementById('thumbnailControls').style.visibility = 'visible';
document.getElementById('controlLEFT').style.visibility = 'visible';
document.getElementById('controlRIGHT').style.visibility = 'visible';
document.getElementById('controlCLOSE').style.visibility = 'visible';
document.getElementById('thumbnail').style.visibility = 'visible';

$('#thumbnailPhoto').empty();
$('#thumbnailPhoto').append(image07);
};

/*==========================================================*/

function openImage08(){

document.getElementById('thumbnailPhoto').style.visibility = 'visible';
document.getElementById('thumbnailControls').style.visibility = 'visible';
document.getElementById('controlLEFT').style.visibility = 'visible';
document.getElementById('controlRIGHT').style.visibility = 'visible';
document.getElementById('controlCLOSE').style.visibility = 'visible';
document.getElementById('thumbnail').style.visibility = 'visible';

$('#thumbnailPhoto').empty();
$('#thumbnailPhoto').append(image08);
};

/*==========================================================*/

function openImage09(){

document.getElementById('thumbnailPhoto').style.visibility = 'visible';
document.getElementById('thumbnailControls').style.visibility = 'visible';
document.getElementById('controlLEFT').style.visibility = 'visible';
document.getElementById('controlRIGHT').style.visibility = 'visible';
document.getElementById('controlCLOSE').style.visibility = 'visible';
document.getElementById('thumbnail').style.visibility = 'visible';

$('#thumbnailPhoto').empty();
$('#thumbnailPhoto').append(image09);
};

/*==========================================================*/

function openImage10(){

document.getElementById('thumbnailPhoto').style.visibility = 'visible';
document.getElementById('thumbnailControls').style.visibility = 'visible';
document.getElementById('controlLEFT').style.visibility = 'visible';
document.getElementById('controlRIGHT').style.visibility = 'hidden';
document.getElementById('controlCLOSE').style.visibility = 'visible';
document.getElementById('thumbnail').style.visibility = 'visible';

$('#thumbnailPhoto').empty();
$('#thumbnailPhoto').append(image10);
};