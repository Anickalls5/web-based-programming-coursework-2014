from flask import Flask, render_template
from flask import request
from datetime import date
from datetime import timedelta
import csv

app = Flask(__name__)

def readFile(aFile):
    with open(aFile,'r') as inFile:
        reader = csv.reader(inFile)
        commentBook = [row for row in reader]
    return commentBook
	
	
def writeFile(aList, aFile):
    with open(aFile,'w',newline='') as outFile:
        writer = csv.writer(outFile)
        writer.writerows(aList)
    return	
	
	
@app.route('/')
def showHome():
    return render_template('home.html')

	
@app.route('/map')
def showMap():
    return render_template('map.html')

	
@app.route('/localities')
def showLocal():
    return render_template('localities.html')

	
@app.route('/regulations')
def showReg():
    return render_template('regulations.html')

	
@app.route('/contact')
def showContact():
    return render_template('contact.html')

	
@app.route('/guestbook', methods=['GET'])
def showGuestbook():
    commentFile = 'static/comments.csv'
    commentBook = readFile(commentFile)
	
    return render_template('guestbook.html',commentBook=commentBook)	
	
		
@app.route('/addComment', methods=['POST'])
def addComment():
    commentFile = 'static/comments.csv'
    commentBook = readFile(commentFile)

    userTitle = request.form[('titleSelect')]
    userName = str(request.form[('userName')])
    commentDate = str(date.today())
    commentText = request.form[('comment')]
    starNumber = request.form[('starNumber')]
	
    if userName == '':
        newComment = ['Anonymous says:', commentText, 'They rated it '+starNumber+' Stars', 'Commented on: '+commentDate]
    else:
        newComment = [userTitle+' '+userName+' says:', commentText, 'They rated it '+starNumber+' Stars', 'Commented on: '+commentDate]
    
    commentBook.append(newComment)
	
    writeFile(commentBook, commentFile)
    return render_template('guestbook.html', commentBook=commentBook, userTitle=userTitle, userName=userName, commentDate=commentDate, commentText=commentText, starNumber=starNumber)
	
	
@app.route('/book')
def getUnavailableDates():
	
    inFile = open('static/bookings.csv','r')
    inFileReader = csv.reader(inFile)
    next(inFileReader)
	
    dateList = ''
    confirmList = ''
    x = 0 

    for row in inFile:
        rowList = row.split(',')
        
        if x == 0:
            dateList += '"'+str(rowList[0])+'"'
            confirmList += '"'+str(rowList[2])+'"'
        else:
            dateList += ', "'+str(rowList[0])+'"'
            confirmList += ', "'+str(rowList[2])+'"'
		    
        x += 1
	
    return render_template('bookPage.html',dateList=dateList,confirmList=confirmList)

	
@app.route('/bookRequest', methods=['POST'])
def postBookRequest():
    
    bookingFile = 'static/bookings.csv'
    bookings = readFile(bookingFile)
	
    firstName = request.form[('forename')]
    lastName = request.form[('surname')]
    email = request.form[('email')]
    startDate = request.form[('dateSelect')]
    duration = int(request.form[('duration')])
    costSeason = int(request.form[('costSeason')])
	
    totalCost = costSeason*duration
	
    startYear = startDate[0]+startDate[1]+startDate[2]+startDate[3]
    startMonth = startDate[5]+startDate[6]
    startDay = startDate[8]+startDate[9]
	
    startDate = date(int(startYear), int(startMonth), int(startDay))
	
    for n in range(duration):
	 	
        bookedDate = startDate + timedelta(days = n)
	
        newBooking = [bookedDate, firstName+' '+lastName, 'N', email, '£'+str(totalCost)+'.00']

        bookings.append(newBooking)
        writeFile(bookings, bookingFile)

        n += 1		
	
    return render_template('bookConfirm.html')

if __name__=='__main__':
    app.run(debug = True)